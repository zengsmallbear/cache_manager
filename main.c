/*
 * Copyright(C) 2020 Dragonsoft. All rights reserved.
 */ 
/*
 * main.c         
 * Original Author:  zengjh@dragonsoft.com.cn, 2020-7-6  
 * 
 * 串行FLASH数据缓冲区的管理，当从芯片读取数据时，内核先从高速缓冲区中读取，
 * 如果在高速缓冲区存在，则不必从芯片中读取数据，如果高速缓冲不存在该数据，
 * 内核会从芯片读取，然后将尽可能多的有效数据保存到高速缓存区中，方便下次读取。
 * 
 * History   
 *   
 *   v1.1     zengjh@dragonsoft.comf.cn        2020-7-10
 *            数据缓冲区管理，数据存在高速缓冲内核则直接读取，
 *            不存在就从芯片读取然后存入高速缓冲,方便下次读取。
 */

#include <stdio.h>
#include <stdlib.h>
#include "buf_manage.h"

int main(void)
{
    int i;
    unsigned int dev, blk_nr;
    unsigned long max_cache;
    struct nand_block_head_s *new_buf;

    (void)printf("请输入缓冲区数目：");
    scanf("%d", &max_cache);    
    nand_init_blk_pool(max_cache);               
    while (1) {
        (void)printf("请输入设备号和块号：");
        scanf("%d %d", &dev, &blk_nr); 
        new_buf = nand_get_block(dev, blk_nr);
        if (new_buf) {
            if (new_buf->flags == DATA_VALID) {
                printf("检索的缓冲区 dev = %d, blk_nr = %d, data = %s\n", 
                        new_buf->dev, new_buf->blk_nr, new_buf->blk_data);
                nand_put_block(new_buf);
            } else {
                (void)printf("请输入要写入的数据：");
                scanf("%s", new_buf->blk_data);
                nand_write_data(new_buf, new_buf->blk_data); 
                (void)nand_show_hash();               
            }
        } else {
            (void)printf("申请失败\n");
            break;
        }  
    }
    (void)nand_free_block();

    return 0;
}
