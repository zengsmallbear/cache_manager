#ifndef _BUF_MANAGE_
#define _BUF_MANAGE_
#include "list.h"

#define DATA_VALID        1	        /* If data is valid */
#define DATA_INVALID      0         /* if data is invalid */
#define BLK_SIZE		  512		/* the chip block size */
#define HASH_BITS         6
#define HASH_SIZE         (1UL << HASH_BITS)
#define HASH_MASK         (HASH_SIZE - 1)

struct nand_block_head_s {
    struct list_head hash;          /* hash链接因子 */
    struct list_head lru;           /* lru链接因子 */
    int dev;                        /* 设备号 */
    int blk_nr;                     /* 块号 */
    int flags;	     	            /* 数据块数据有效标志 */
    unsigned char blk_data[0];      /* 存储数据块 */
}; 

void nand_init_blk_pool(unsigned long max_cache);                           /* 初始化空闲缓冲区 */
struct nand_block_head_s *nand_get_block(int dev, int blk_nr);              /* 分配缓存区 */
void nand_put_block(struct nand_block_head_s *tmp_blk_node);                /* 释放缓冲区 */ 
void nand_write_data(struct nand_block_head_s *tmp_blk_bode, char *data);   /* 写入数据 */
void nand_show_hash();                                                      /* 打印hash表 */
void nand_free_block();                                                     /* 释放缓冲区 */

#endif  /* _BUF_MANAGE_ */
